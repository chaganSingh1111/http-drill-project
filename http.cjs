const http = require("http");
const crypto = require("crypto");
const fs = require("fs");
const jsonPath = require("./slidshow.json");

const server = http.createServer((req, res) => {
  if (req.method === "GET") {
    if (req.url === "/html") {

      res.setHeader("Content-Type", "text/html");
      res.statusCode = 200;
      fs.readFile("./index.html", (err, data) => {
        if (err) {
          console.log(err);
        } else {
          res.end(data);
        }
      });
    } else if (req.url === "/json") {

      res.setHeader("Content-Type", "application/json");
      res.statusCode = 200;
      res.end(JSON.stringify(jsonPath));

    } else if (req.url === "/uuid") {

      res.setHeader("Content-Type", "application/json");
      res.statusCode = 200;
      res.end(
        JSON.stringify({
          uuid: crypto.randomUUID(),
        })
      );
      
    } else if (req.url.startsWith("/status/")) {
      const statusCode = req.url.split("/")[2];
      const intData = parseInt(statusCode)
      if (Number.isInteger(intData)) {
        res.statusCode = intData;
        res.end(`Response with status code: ${statusCode}`);
      } else {
        res.statusCode = 400;
        res.end("Invalid");
      }

    }else if (req.url.startsWith('/delay/')) {

      const delayInSeconds = req.url.split('/')[2];
      const delayInt = parseInt(delayInSeconds)

      if (Number.isInteger(delayInt)) {
        setTimeout(() => {
          res.statusCode = 200;
          res.end('Response after delay');
        }, delayInt* 1000);

      } else {
        res.statusCode = 400;
        res.end('Invalid');
      }

    } else {
      res.statusCode = 404;
      res.end('Endpoint not found');
    }


  } else {
    res.statusCode = 405;
    res.end("Method not allowed");
  }
});



const port = 3000;
server.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
